package org.example;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

public class DepartmentsDao {
	private HibernateFactory hibernateFactory = new HibernateFactory();

	public void save(Department department){
		Session session = hibernateFactory.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		session.save(department);
		transaction.commit();
		session.close();
	}

	public List<Department> getAll(){
		Session session = hibernateFactory.getSessionFactory().openSession();
		List<Department> departments = session.createQuery("From Department", Department.class).list();
		session.close();
		return departments;
	}

	public void update(Department location){
		Session session = hibernateFactory.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		session.update(location);
		transaction.commit();
		session.close();
	}

	public Department get(int id){
		Session session = hibernateFactory.getSessionFactory().openSession();
		Department location = session.get(Department.class, id);
		session.close();
		return location;
	}

	public void delete(Department location){
		Session session = hibernateFactory.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		session.delete(location);
		transaction.commit();
		session.close();
	}

	public void deletebyId(int id){
		Session session = hibernateFactory.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		Department location = session.get(Department.class, id);
		session.delete(location);
		transaction.commit();
		session.close();
	}
}
