package org.example;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

public class LocationsDao {
	private HibernateFactory hibernateFactory = new HibernateFactory();

	public void save(Location location){
		Session session = hibernateFactory.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		session.save(location);
		transaction.commit();
		session.close();
	}

	public List<Location> getAll(){
		Session session = hibernateFactory.getSessionFactory().openSession();
		List<Location> locations = session.createQuery("From Location", Location.class).list();
		session.close();
		return locations;
	}

	public void update(Location location){
		Session session = hibernateFactory.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		session.update(location);
		transaction.commit();
		session.close();
	}

	public Location get(int id){
		Session session = hibernateFactory.getSessionFactory().openSession();
		Location location = session.get(Location.class, id);
		session.close();
		return location;
	}

	public void delete(Location location){
		Session session = hibernateFactory.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		session.delete(location);
		transaction.commit();
		session.close();
	}

	public void deletebyId(int id){
		Session session = hibernateFactory.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		Location location = session.get(Location.class, id);
		session.delete(location);
		transaction.commit();
		session.close();
	}
}
