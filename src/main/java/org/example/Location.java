package org.example;

import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "LOCATIONS")
public class Location {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "LOCATION_ID")
	private int locationId;

	@Column(name = "STREET_ADDRESS", unique = false)
	private String streetAddress;

	@Column(name = "POSTAL_CODE")
	private String postalCode;

	@Column(name = "CITY")
	private String city;

	@Column(name = "STATE_PROVINCE")
	private String stateProvince;

	@Column(name = "COUNTRY_ID")
	int countryId;

	@OneToMany(mappedBy = "locationInDepartment")
	private List<Department> departments;

	public Location() {
	}

	public Location(String streetAddress, String postalCode, String city, String stateProvince, int countryId, List<Department> departments) {
		this.streetAddress = streetAddress;
		this.postalCode = postalCode;
		this.city = city;
		this.stateProvince = stateProvince;
		this.countryId = countryId;
		this.departments = departments;
	}

	public int getLocationId() {
		return locationId;
	}

	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStateProvince() {
		return stateProvince;
	}

	public void setStateProvince(String stateProvince) {
		this.stateProvince = stateProvince;
	}

	public int getCountryId() {
		return countryId;
	}

	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}

	public List<Department> getDepartments() {
		return departments;
	}

	public void setDepartments(List<Department> departments) {
		this.departments = departments;
	}

	@Override
	public String toString() {
		return "Location{" +
				   "locationId=" + locationId +
				   ", streetAddress='" + streetAddress + '\'' +
				   ", postalCode='" + postalCode + '\'' +
				   ", city='" + city + '\'' +
				   ", stateProvince='" + stateProvince + '\'' +
				   ", countryId=" + countryId +
				   '}';
	}
}
