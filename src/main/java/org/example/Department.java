package org.example;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Departments")
public class Department {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "DEPARTMENT_ID")
	private int departmentId;

	@Column(name = "DEPARTMENT_NAME")
	private String departmentName;

	@ManyToOne
	private Location locationInDepartment;

	public Department() {
	}

	public Department(String departmentName, Location locationInDepartment) {
		this.departmentName = departmentName;
		this.locationInDepartment = locationInDepartment;
	}

	public int getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public Location getLocationInDepartment() {
		return locationInDepartment;
	}

	public void setLocationInDepartment(Location locationInDepartment) {
		this.locationInDepartment = locationInDepartment;
	}

	@Override
	public String toString() {
		return "Department{" +
				   "departmentId=" + departmentId +
				   ", departmentName='" + departmentName + '\'' +
				   ", locationInDepartment=" + locationInDepartment +
				   '}';
	}
}

