package org.example;

import java.util.Arrays;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Location gdansk = new Location();
        gdansk.setCity("Gdansk");
        gdansk.setPostalCode("80-800");
        gdansk.setStreetAddress("Gdanska");

        Department backendDevs = new Department();
        backendDevs.setDepartmentName("Backend developers");
        backendDevs.setLocationInDepartment(gdansk);

        Department frontendDevs = new Department();
        frontendDevs.setDepartmentName("Frontend developers");
        frontendDevs.setLocationInDepartment(gdansk);

        List<Department> departments = Arrays.asList(backendDevs, frontendDevs);
        gdansk.setDepartments(departments);

        LocationsDao locationsDao = new LocationsDao();
        locationsDao.save(gdansk);

        locationsDao.getAll().forEach(System.out::println);
        DepartmentsDao departmentsDao = new DepartmentsDao();
        departmentsDao.save(backendDevs);
        departmentsDao.save(frontendDevs);
        departmentsDao.getAll().forEach(System.out::println);

//        DepartmentsDao departmentsDao = new DepartmentsDao();
//        departmentsDao.save(backendDevs);

//        Location gdansk = new Location();
//        gdansk.setCity("Gdansk");
//        gdansk.setPostalCode("80-800");
//        gdansk.setStreetAddress("Gdanska");
//
//        LocationsDao locationsDao = new LocationsDao();
//        locationsDao.save(gdansk);
//        locationsDao.getAll().forEach(System.out::println);

    }
}
